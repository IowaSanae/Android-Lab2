import 'dart:core';

mixin CommonValidation {
  String? emailValidator(String? value) {
    if (!value!.contains('@')) {
      return "Please input a valid email address!";
    }

    return null;
  }

  String? nameValidator(String? value) {
    String invalid = "You have a name... right?";
    if(value!.isEmpty) {
      return invalid;
    }

    return null;
  }

  String? birthYearValidator(String? value) {
    String invalid = "$value is not your real birth year";

    if(int.parse(value!).isNaN || int.parse(value) <= 1850 || int.parse(value) >= 2022) {
      return invalid;
    }

    return null;
  }

  String? addressValidator(String? value) {
    String invalid = "Consider having a roof over your head before playing on your phone.";
    if (value!.isEmpty) {
      return invalid;
    }

    return null;
  }
}