import 'package:flutter/material.dart';
import 'package:validation_form/validators/mixin_validator.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Register",
      home: Scaffold(
        appBar: AppBar(title: Text("Register")),
        body: RegisterScreen(),
      ),
    );
  }
}

class RegisterScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return RegisterState();
  }
}

class RegisterState extends State<StatefulWidget> with CommonValidation {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String familyAndMiddleName;
  late String givenName;
  late int birthYear;
  late String address;

  // Creates a register form
  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        children: [
          emailAddressField(),
          Container(margin: const EdgeInsets.only(top: 20.0),),
          familyAndMiddleNameField(),
          Container(margin: const EdgeInsets.only(top: 20.0),),
          givenNameField(),
          Container(margin: const EdgeInsets.only(top: 20.0),),
          birthYearField(),
          Container(margin: const EdgeInsets.only(top: 20.0),),
          addressField(),
          Container(margin: const EdgeInsets.only(top: 40.0),),
          loginButton()
        ],
      )
    );
  }

  Widget emailAddressField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: const InputDecoration(
          icon: Icon(Icons.alternate_email),
          labelText: "Email address"
      ),
      validator: emailValidator,
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget familyAndMiddleNameField() {
    return TextFormField(
      keyboardType: TextInputType.name,
      decoration: const InputDecoration(
        icon: Icon(Icons.account_box),
        labelText: "Family and Middle Name"
      ),
      validator: nameValidator,
      onSaved: (value) {
        familyAndMiddleName = value as String;
      }
    );
  }

  Widget givenNameField() {
    return TextFormField(
        keyboardType: TextInputType.name,
        decoration: const InputDecoration(
            icon: Icon(Icons.person),
            labelText: "Given Name"
        ),
        validator: nameValidator,
        onSaved: (value) {
          givenName = value as String;
        }
    );
  }

  Widget birthYearField() {
    return TextFormField(
        keyboardType: TextInputType.number,
        decoration: const InputDecoration(
            icon: Icon(Icons.numbers),
            labelText: "Birth Year"
        ),
        validator: birthYearValidator,
        onSaved: (value) {
          birthYear = value as int;
        }
    );
  }

  Widget addressField() {
    return TextFormField(
        keyboardType: TextInputType.name,
        decoration: const InputDecoration(
            icon: Icon(Icons.house),
            labelText: "Address"
        ),
        validator: addressValidator,
        onSaved: (value) {
          address = value as String;
        }
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, $familyAndMiddleName, $givenName, $birthYear, $address');
          }
        },
        child: const Text("Register")
    );
  }
}
